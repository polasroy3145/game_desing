import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(MyApp());
}
class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int leftCardNumber =3;
  int rightCardNumber =4;
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("assets/images/background.jpg")
            )
          ),
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset("assets/images/logo.jpeg",width: 100,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset("assets/images/card$leftCardNumber.png",width: 100,),
                    Image.asset("assets/images/card$rightCardNumber.png",width: 100,),
                    
                  ],
                ),
                TextButton(onPressed: (){
                  setState(() {
                    leftCardNumber=5;
                    rightCardNumber=7;
                  });
                  print("pressed");


                }, child: Image.asset("assets/images/dealbutton.png")),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Text("Plyer",style: TextStyle(color: Colors.white,fontSize: 20),),
                        SizedBox(height: 6,),
                        Text("0",style: TextStyle(color: Colors.white,fontSize: 20),),

                      ],
                    ),
                    Column(
                      children: [
                        Text("Plyer",style: TextStyle(color: Colors.white,fontSize: 20),),
                        SizedBox(height: 6,),
                        Text("0",style: TextStyle(color: Colors.white,fontSize: 20),),

                      ],
                    )
                  ],
                )

              ],),
          ),


        ),
      ),
    );
  }
}
